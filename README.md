# GitLab CI Templates

Templates for GitLab CI

## Docker [Buildkit](https://github.com/moby/buildkit)

Builds a Dockerfile & pushes to GitLab's Container registry (tagged with commit-sha & `latest`)
Since Docker imposed more strict pull limits, we set up mirror.gcr.io as default registry mirror.

```yaml
include:
  - project: cmdjulian/git-lab-ci-container-building-templates # https://gitlab.com/cmdjulian/git-lab-ci-container-building-templates
    file: buildkit.gitlab-ci.yml
    # ref: vX.Y - use a tag as fixed version (pro tip: renovate bot will detect upgrades)

container-image:
  extends: .buildkit # or to use GitLab dependency proxy: .buildkit_dependencyproxy
  # < CUSTOMIZATIONS HERE > #
```

### Customize Docker context

```yaml
variables:
  DOCKER_CONTEXT: everything-docker-needs/
  DOCKERFILE_DIR: docker-stuff/
  DOCKERFILE_PATH: Custom.dockerfile
```

### Customize push

Default rules are to always push with commit-sha & `latest` tag only on default branch.

Override `rules` if you want something different, e.g.:

```yaml
rules:
  - if: '$CI_COMMIT_BRANCH == "dev"'
    variables:
      PUSH_IMAGES: 'name=$CI_REGISTRY_IMAGE:$CI_COMMIT_SHA,$CI_REGISTRY_IMAGE:dev'
```

By default, the image is pushed. If you do want to skip pushing, you can set `PUSH=false`.

Push images are by default `$CI_REGISTRY_IMAGE:$CI_COMMIT_SHA` and `$CI_REGISTRY_IMAGE:latest`. Images can be customized
by overriding the `PUSH_IMAGES` variable. Images have to be seperated by a new line:

```yaml
variables:
  PUSH_IMAGES: |-
    docker.io/foosername/hello-world:latest
    docker.io/foosername/hello-world:dev
```

Single images can be written like:

```yaml
variables:
  PUSH_IMAGES: docker.io/foosername/hello-world:latest
```

#### Registries

This Job always logs into gitlab container registry.

To set an additional Registry, set the following variables:

- `REGISTRY_URL`: `https://index.docker.io/v1/`
- `REGISTRY_USER`: `foosername`
- `REGISTRY_PASSWORD`: `prefer-access-token`

`REGISTRY_URL` defaults to `https://index.docker.io/v1/`. So to additionally login into docker hub just supply username
and password.  
If to login is determined by the presence of `REGISTRY_USER` variable.

```yaml
variables:
  PUSH_IMAGES: docker.io/foosername/hello-world:latest
```

#### Additional Custom registry

```yaml
variables:
  PUSH_IMAGES: registry.my.org/foosername/hello-world:$CI_COMMIT_SHA
before_script:
  - !reference [ .buildkit, before_script ]
  # My Registry proxy login
  - >
    cat ~/.docker/config.json | 
    jq ".auths += {\"registry.my.org\": {
      auth: \"$(echo -n "$MY_REGISTRY_USER:$MY_REGISTRY_PASSWORD" | base64)\"
    }}" | sponge ~/.docker/config.json
```

### Customize script before build

```yaml
script:
  # Add version to commit.txt
  - echo "$CI_COMMIT_SHA" > commit.txt
  - !reference [ .buildkit, script ]
```

### Cache

Default is inline cache. The cache is imported from the first appearing image.:

```yaml
variables:
  PUSH_IMAGES: |-
    $CI_REGISTRY_IMAGE:distroless-static
    $CI_REGISTRY_IMAGE:latest
  DOCKER_USE_INLINE_CACHE: 'true'
```

In that case, `$CI_REGISTRY_IMAGE:distroless-static` is picked.

You can also use a dedicated cache image. This can be done by disabling the inline cache and specifying a cache-image:

```yaml
variables:
  PUSH_IMAGES: |-
    $CI_REGISTRY_IMAGE:distroless-static
    $CI_REGISTRY_IMAGE:latest
  DOCKER_USE_INLINE_CACHE: 'false'
  DOCKER_CACHE_IMAGE: $CI_REGISTRY_IMAGE/cache:latest
```

### Customize target platform/architecture

See [buildkit docs](https://github.com/moby/buildkit/blob/master/docs/multi-platform.md)

```yaml
variables:
  TARGET_PLATFORMS: |-
    linux/arm/v7
    linux/amd64
```

Platforms have to be split by newlines. If only one platform is required, you can also write a single scalar value:

```yaml
variables:
  TARGET_PLATFORMS: linux/arm64 
```

### Labels

Additional labels can be added via the `LABELS` env var. Label entries have to be a valid key-value pair and are split
by newlines.

```yaml
variables:
  LABELS: |-
    foo=bar
    fizz=buzz
```

### Build-Args

Build-Args can be added via the `BUILD_ARGS` env var. Build-Args have to be a valid key-value pair and are split by
newlines.

```yaml
variables:
  BUILD_ARGS: |-
    foo=bar
    fizz=buzz
```

### Secrets

Secrets can be added via the `SECRETS` env var. Secrets have to be a valid secret param and are split by newlines.

```yaml
variables:
  SECRETS: |-
    id=secretname,src=filepath
    id=foo,src=./bar
```

To create a secret you can use the `before_script` block.

### Additional buildkit arguments

```yaml
variables:
  BUILDKIT_ARGS: >-
    --opt foo=bar
    --opt fizz=buzz
```

### Debug

By setting the `DEBUG` variable to `true`, buildkit-args are echoed to the cli.